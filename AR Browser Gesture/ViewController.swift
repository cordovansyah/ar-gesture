//
//  ViewController.swift
//  AR Browser Gesture
//
//  Created by octagon studio on 05/10/18.
//  Copyright © 2018 Cordova. All rights reserved.
//

import ARKit
import Vision
import WebKit

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSCNView!

    
    private let serialQueue = DispatchQueue(label: "com.cordova.broswerar")
    private var visionRequests = [VNRequest]()
    private let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: 640, height: 480))
    
    var textResult = UILabel(frame: CGRect(x: 16, y: 500, width: 343, height: 84))
    let textContainer = UIView(frame: CGRect(x: 16, y: 500, width: 343, height: 84))
 
    //Hide Status Bar
    override var prefersStatusBarHidden : Bool { return true }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForAR()
        setupForML()
        updateCoreMLClassificationStream()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            
        }
    }
    
    //MARK: -Setup for AR and ML
    
    func setupForAR(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
        
        let ARSessionConfiguration = ARWorldTrackingConfiguration()
        sceneView.session.run(ARSessionConfiguration)
    }
    
    //Tapped Function for Setup AR by Building The Video Screen
    @objc func tapped(recognizer: UIGestureRecognizer) {
        guard let currentVideoFramePlaying = self.sceneView.session.currentFrame,
            let requestToWeb = URL(string: "https://www.supercharge.io") else {
                return
        }
        
        webView.load(URLRequest(url: requestToWeb))
        
        let browserCanvas = SCNPlane(width: 1.0, height: 0.75)
        browserCanvas.firstMaterial?.diffuse.contents = webView
        browserCanvas.firstMaterial?.isDoubleSided = true
        
        let browserCanvasNode = SCNNode(geometry: browserCanvas)
        var matrixTranslationforBrowser = matrix_identity_float4x4
        matrixTranslationforBrowser.columns.3.z = -1.0
        
        browserCanvasNode.simdTransform = matrix_multiply(currentVideoFramePlaying.camera.transform, matrixTranslationforBrowser)
        browserCanvasNode.eulerAngles = SCNVector3Zero
        self.sceneView.scene.rootNode.addChildNode(browserCanvasNode)
    }
    
    
    func setupForML(){
        guard let activeMLModel = try? VNCoreMLModel(for: example_5s0_hand_model().model) else {
            fatalError("Load model failed, please retry")
        }
        
        let gestureClassificationRequest = VNCoreMLRequest(model: activeMLModel, completionHandler: gestureClassificationHandler)
        
        visionRequests = [gestureClassificationRequest]
    }
    
    
    func classifierResult(){
        textResult.textAlignment = .center
        textResult.text = ""
        textResult.textColor = UIColor.white
        textResult.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.light)
        self.view.addSubview(textResult)
    }
    
    func drawTextContainer(){
        textContainer.backgroundColor = UIColor.black.withAlphaComponent(0.60)
        textContainer.isOpaque = false
        textContainer.layer.cornerRadius = 25
        self.view.addSubview(textContainer)
    }
    
    func updateCoreMLClassificationStream(){
        serialQueue.async {
            self.updateCoreML()
            self.updateCoreMLClassificationStream()
        }
    }
    
    func updateCoreML(){
        let pixbuff : CVPixelBuffer? = (sceneView.session.currentFrame?.capturedImage)
        if pixbuff == nil { return }
        let ciImage = CIImage(cvPixelBuffer: pixbuff!)
        
        // Prepare CoreML/Vision Request
        let imageRequestHandler = VNImageRequestHandler(ciImage: ciImage, options: [:])
        
        // Run Vision Image Request
        do {
            try imageRequestHandler.perform(self.visionRequests)
        } catch {
            print(error)
        }
    }
    
    
    func gestureClassificationHandler(request: VNRequest, error: Error?){
        if error != nil {
            print("Error: " + (error?.localizedDescription)!) //translate localized error in console
            return
        }
        guard let observation = request.results else {
            print("No results")
            return
        }
        
        let classifications = observation[0...2]
            .compactMap({ $0 as? VNClassificationObservation})
            .map({ "\($0.identifier) \(String(format:" : %.2f", $0.confidence))" })
            .joined(separator: "\n")
        
        //Render Gestures
        DispatchQueue.main.async {
            let topPrediction = classifications.components(separatedBy: "\n")[0]
            let topPredictionName = topPrediction.components(separatedBy: ":")[0].trimmingCharacters(in: .whitespaces)
            
            let topPredictionScore:Float? = Float(topPrediction.components(separatedBy: ":")[1].trimmingCharacters(in: .whitespaces))
            
            if (topPredictionScore != nil && topPredictionScore! > 0.10) {
                if (topPredictionName == "fist-UB-RHand") {
                    var scrollHeight: CGFloat = self.webView.scrollView.contentSize.height - self.webView.bounds.size.height
                    if (0.0 > scrollHeight){
                        scrollHeight = 0.0
                    }
                    self.webView.scrollView.setContentOffset(CGPoint(x: 0.0, y: scrollHeight), animated: true)
                }
                
                if (topPredictionName == "FIVE-UB-RHand") {
                     self.webView.scrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
                }
            }
            
           
        }
        
    }
}
