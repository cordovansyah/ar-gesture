# Augmented Reality Gesture ( AR Based User Interaction )

This is a project that comes out of my curiosity about having someone scroll or click a virtual based object as in Sci Fi movies. Thus, 
I decided to learn and build one by implementing ARKit and CoreML to recognize gesture and connect with AR based interaction, and in this case, 
a simple scrolling interaction.

# Motivation
I love to learn and build stuffs, and currently we're entering the AI First world where everything shall be gradually automated, including
the way we think and build apps for both mobile and web. I am keen to learn from widely preserved references about harnessing the power of
Artificial Intelligence in building a useful app for anyone to learn and adapt to the technological growth, and for sure, looking forward to
be part of a greater team for behemothal contribution towards global society, through technology. 


